class Arbeiter extends Arbeitnehmer {
    constructor(name,abteilung,stundenlohn) {
        super(name,abteilung)
        this.stundenlohn = stundenlohn
    }

    toString() {
        return super.toString() + `<br>Stundenlohn: ${this.stundenlohn}`;
    }
}

